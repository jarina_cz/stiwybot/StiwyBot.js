# StiwyBot

Simple Discord Bot written in Javascript

## Installation

```sh
git clone https://gitlab.com/jarina_cz/stiwybot/StiwyBot.js.git
cd StiwyBot.js
npm install
```

* Audio capabilities

Based on your operating system you might need to install particular packages globally.

```sh
npm install -g node-opus   # Requires python executable
npm install -g ffmpeg
```

### Known Issues

#### Python Executable can't be found on Windows

This may appear while building node-opus package
Should help just install Python from [python.org](https://www.python.org/)<br>
If that won't help however, you can do following:

```powershell
npm --add-python-to-path='true' --debug install --global windows-build-tools
```

#### FFMPEG not found

##### Windows

Via [Choco](https://chocolatey.org/)

```sh
choco install ffmpeg
```

Or use standard Windows Installer from [ffmpeg.org](https://www.ffmpeg.org/)

##### Mac OS X

Via [Homebrew](https://brew.sh/)

```sh
brew install ffmpeg
```

##### Linux

Via package manager for your distribution

```sh
sudo apt install ffmpeg  # Ubuntu/Debian etc.
sudo yum install ffmpeg  # Red-Hat/Fedora/Suse etc.
sudo pacman -Sy ffmpeg   # Archlinux/Manjaro etc.
```

## Run

### Set up environment variable

```sh
# Linux/Mac OS X
export DISCORD_BOT_TOKEN_STIWY_BOT="your_discord_token"

# Windows
set DISCORD_BOT_TOKEN_STIWY_BOT="your_discord_token"      # CMD
$Env:DISCORD_BOT_TOKEN_STIWY_BOT = "your_discord_token"   # PowerShell
```

### Local Development

```sh
npm run dev         # linux/macos 
```

### Production

#### NOW-SH

* Deployment to now.sh including ENV variables

```sh
now -e NODE_ENV=production -e DISCORD_BOT_TOKEN_STIWY_BOT="your_discord_token"
```

* Alternatively you can use [now.sh](https://zeit.co/now) secrets

```sh
now secret add DISCORD_BOT_TOKEN_STIWY_BOT "your_discord_token"
now -e NODE_ENV=production -e DISCORD_BOT_TOKEN_STIWY_BOT=@discord_bot_token_stiwy_bot
```

* By default scaling is set to automatically turn off your app if there is no traffic. You can override this as follows:

```sh
now scale deployment_name 1  # set to minimum of 1 instance
```
