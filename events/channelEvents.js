module.exports = (client, logger) => {
  client.on('channelCreate', (channel) => {
    if (channel.type === 'dm') return
    logger.info(`A ${channel.type} channel '${channel.name}' was created'`)
    if (channel.type === 'text') channel.send('Welcome to your new channel.')
  })

  client.on('channelDelete', (channel) => {
    logger.info(`A ${channel.type} channel '${channel.name}' was deleted'`)
  })
}
