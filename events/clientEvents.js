module.exports = (client, logger) => {
  client.on('ready', () => {
    logger.info(`Logged in as ${client.user.username} - (${client.user.id})`)
  })

  client.on('disconnect', () => {
    logger.error('You have been disconnected')
  })

  client.on('reconnecting', () => {
    logger.warn('Reconnecting')
  })
}
