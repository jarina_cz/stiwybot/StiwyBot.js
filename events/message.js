const settings = require('../settings.json')
const profanityFilter = require('../util/profanityFilter')

const repliesDispatcher = require('./replies')

module.exports = (msg, logger) => {
  // Ignore DM channel messages
  if (msg.channel.type !== 'text') return
  // If message sent by bot itself, don't do anything
  if (msg.author.bot) return

  // control channels for swear words (including commands)
  if (settings.profanityfilter) {
    const swearWords = new RegExp(profanityFilter)
    if (swearWords.test(msg.content)) {
      msg.delete()
        .then((msg) => {
          logger.info(`Edited message from ${msg.author}`)
          msg.channel.send(`Last message by ${msg.author.username} removed by StiwyBot for using mature language`)
        })
        .catch((err) => logger.error(err))
      return
    }
  }

  // Treat prefixed messages as a commands
  if (msg.content.startsWith(settings.prefix) && !msg.content.startsWith(';;')) {
    const client = msg.client
    let command = msg.content.split(' ')[0].slice(settings.prefix.length)
    let params = msg.content.split(' ').slice(1)
    let perms = client.elevation(msg)
    let cmd

    if (client.commands.has(command)) {
      cmd = client.commands.get(command)
    } else if (client.aliases.has(command)) {
      cmd = client.commands.get(client.aliases.get(command))
    } else {
      msg.channel.send(`${msg.author} I don't recognize that command. Try ';help' to see what I can do.`)
    }

    if (cmd) {
      if (perms < cmd.conf.permLevel) return
      cmd.run(logger, client, msg, params, perms)
    }
  } else {
    if (msg.mentions.users.first() && msg.mentions.users.first().bot) {
      let message = msg.content.replace('<@!' + msg.mentions.users.first().id + '>', '')
      repliesDispatcher(msg, message, logger)
    }
  }
}
