module.exports = (client, logger) => {
  client.on('guildMemberAdd', (member) => {
    let guild = member.guild
    guild.defaultChannel.send(`Please welcome ${member.user.username} to the server`)
    logger.info(`User ${member.user.username} joined ${guild.name}`)
  })

  client.on('guildBanAdd', (guild, user) => {
    guild.defaultChannel.send(`${user.username} was just banned!`)
  })

  client.on('guildBanRemove', (guild, user) => {
    guild.defaultChannel.send(`${user.username} was just unbanned!`)
  })
}
