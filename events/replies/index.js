const fs = require('fs')

module.exports = function (msg, message, logger) {
  fs.readdir('./events/replies/', (err, files) => {
    if (err) {
      logger.error(err)
    }
    files.forEach(f => {
      if (f !== 'index.js') {
        let props = require(`./${f}`)
        for (let rules of props) {
          let answer = rules.answer.replace('<author>', msg.author)
          if (message.match(rules.question)) msg.channel.send(answer)
        }
      }
    })
  })
}
