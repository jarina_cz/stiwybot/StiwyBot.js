module.exports = [
  {
    question: /hi|hello/i,
    answer: `Hello <author>`
  },
  {
    question: /How are you/i,
    answer: `<author> I'm fine, thanks for asking :-) How about you?`
  },
  {
    question: /How old are you/i,
    answer: `<author> I'm old enough :wink:`
  },
  {
    question: /Are you ? [man|woman]/i,
    answer: `'looks down'... Yeah <author>, definitely a man`
  }
]
