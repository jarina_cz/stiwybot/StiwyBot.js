// const ddiff = require('return-deep-diff')  // Used only with roleUpdate

module.exports = (client, logger) => {
  client.on('roleDelete', (role) => {
    logger.info(`A role ${role.name} has been deleted`)
  })

  client.on('roleCreate', (role) => {
    logger.info(`A new role has been created`)
  })
}
