// These events are here only for learning purpose to showcase functionality

const ddiff = require('return-deep-diff')

module.exports = (client, logger) => {
  // GUILD EVENTS
  client.on('guildDelete', (guild) => {
    logger.info(`I have left ${guild.name}`)
  })

  client.on('guildCreate', (guild) => {
    guild.defaultChannel.send(`I have joined ${guild.name}`)
    logger.info(`I have joined ${guild.name}`)
  })

  // client.on('guildMemberSpeaking', (member, speaking) => {
  //   let guild = member.guild
  //   if (member.speaking) {
  //     guild.defaultChannel.send(`${member.user.username} is speaking!`)
  //   }
  // })

  client.on('guildUpdate', (oGuild, nGuild) => {
    logger.info(ddiff(oGuild, nGuild))
  })

  // MESSAGE EVENTS
  client.on('messageDelete', (msg) => {
    logger.info(`A message with the ID ${msg.id} was deleted from ${msg.channel}`)
  })

  client.on('messageDeleteBulk', (messages) => {
    logger.info(`${messages.size} was deleted`)
  })

  // CHANNEL EVENTS
  client.on('channelUpdate', (oChannel, nChannel) => {
    logger.info(ddiff(oChannel, nChannel))
  })

  // ROLES EVENTS
  // This is spammy as hell
  // client.on('roleUpdate', (oRole, nRole) => {
  //   logger.info(ddiff(oRole, nRole))
  // })
}
