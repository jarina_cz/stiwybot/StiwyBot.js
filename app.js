// imports
const Discord = require('discord.js')
const logger = require('winston')
const fs = require('fs')
const {createServer} = require('http') // now.sh requires listening on port, so this is a workaround

// Configure variables
const settings = require('./settings.json')
const token = process.env.DISCORD_BOT_TOKEN_STIWY_BOT || settings.token

// Configure Winston Logger
logger.remove(logger.transports.Console)
logger.add(logger.transports.Console, {
  colorize: true,
  timestamp: true
})
logger.level = 'debug'

// Initialize Discord Client
const client = new Discord.Client()

// Client Events
require('./events/clientEvents')(client, logger)
// Guild Events
require('./events/guildEvents')(client, logger)
// Channel Events
require('./events/channelEvents')(client, logger)
// Role Events
require('./events/roleEvents')(client, logger)

client.commands = new Discord.Collection()
client.aliases = new Discord.Collection()
fs.readdir('./commands/', (err, files) => {
  if (err) console.error(err)
  logger.info(`Loading a total of ${files.length} commands.`)
  files.forEach(f => {
    let props = require(`./commands/${f}`)
    if (props.conf.enabled) {
      logger.info(`Loading Command: ${props.help.name}. 👌`)
      client.commands.set(props.help.name, props)
      props.conf.aliases.forEach(alias => {
        client.aliases.set(alias, props.help.name)
      })
    } else {
      logger.info(`Ignoring Command: ${props.help.name} -- disabled in config`)
    }
  })
})

client.reload = (command) => {
  return new Promise((resolve, reject) => {
    try {
      delete require.cache[require.resolve(`./commands/${command}`)]
      let cmd = require(`./commands/${command}`)
      client.commands.delete(command)
      client.aliases.forEach((cmd, alias) => {
        if (cmd === command) client.aliases.delete(alias)
      })
      client.commands.set(command, cmd)
      cmd.conf.aliases.forEach(alias => {
        client.aliases.set(alias, cmd.help.name)
      })
      resolve()
    } catch (e) {
      reject(e)
    }
  })
}

client.elevation = (msg) => {
  /* This function should resolve to an ELEVATION level which
     is then sent to the command handler for verification */
  let permlvl = 0

  let modRole = msg.guild.roles.find('name', settings.modrolename)
  if (modRole && msg.member.roles.has(modRole.id)) permlvl = 2

  let adminRole = msg.guild.roles.find('name', settings.adminrolename)
  if (adminRole && msg.member.roles.has(adminRole.id)) permlvl = 3

  if (msg.author.id === settings.ownerid) permlvl = 4

  return permlvl
}

client.on('message', (msg) => {
  require('./events/message')(msg, logger)
})

// Login to Discord
client.login(token)
  .catch(console.error)

// now.sh requires listening on port, so this is a workaround
const server = createServer(() => {})
server.listen(3000)
