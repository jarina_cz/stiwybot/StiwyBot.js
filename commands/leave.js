exports.run = (logger, client, msg, args) => {
  let voiceChannel = msg.member.voiceChannel
  if (!voiceChannel) {
    msg.channel.send('I am not in a voice channel')
  } else {
    msg.channel.send(`Left channel ${voiceChannel.name}`)
      .then(() => {
        voiceChannel.leave()
      })
      .catch((err) => {
        msg.channel.send(err)
      })
  }
}

exports.conf = {
  enabled: process.env.NODE_ENV !== 'production',
  guildOnly: false,
  aliases: [],
  permLevel: 0
}

exports.help = {
  name: 'leave',
  description: 'Leaves your active voice channel',
  usage: 'leave'
}
