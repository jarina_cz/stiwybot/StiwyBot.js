exports.run = (logger, client, msg, args) => {
  let user = msg.mentions.users.first()
  if (!user) return msg.reply('You need to mention someone')
  return msg.channel.send(`There is ${Math.floor(Math.random() * 100) + 1}% love between ${msg.author.username} and ${user}`)
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 0
}

exports.help = {
  name: 'love',
  description: 'Tells you how much in love are you with mentioned user',
  usage: 'love <mention>'
}
