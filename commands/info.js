const Discord = require('discord.js')

exports.run = (logger, client, msg, args) => {
  if (msg.mentions.users.size < 1) {
    return msg.reply('You must mention someone to warn them')
  }
  let user = msg.mentions.users.first()

  const embed = new Discord.RichEmbed()
    .setColor(0x00AE86)
    .setThumbnail(user.displayAvatarURL)
    .addField('Name:', user.username, true)
    .addField('ID:', user.id, true)
    .addField('Status', user.presence.status)

  if (!msg.author.dmChannel) {
    msg.author.createDM()
      .then((channel) => {
        channel.send('', {embed: embed})
      })
      .catch((err) => console.log(err))
  } else {
    msg.author.dmChannel.send('', {embed: embed})
  }
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 0
}

exports.help = {
  name: 'info',
  description: 'Returns an info about user via DM',
  usage: 'info <mention>'
}
