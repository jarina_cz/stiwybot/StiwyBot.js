exports.run = (logger, client, msg, args) => {
  msg.channel.send(
    `Pong! '${Date.now() - msg.createdTimestamp}ms'`
  )
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 0
}

exports.help = {
  name: 'ping',
  description: 'Pings the Bot',
  usage: 'ping'
}
