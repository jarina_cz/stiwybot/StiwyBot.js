exports.run = (logger, client, msg, args) => {
  let newNick = args.join(' ').trim()
  console.log(newNick)
}

exports.conf = {
  enabled: false,
  guildOnly: false,
  aliases: [],
  permLevel: 0
}

exports.help = {
  name: 'nick',
  description: 'Set a new nick',
  usage: 'nick <new_nick>'
}
