const xkcd = require('xkcd-api')
exports.run = (logger, client, msg, args) => {
  xkcd.random(function (error, response) {
    if (error) {
      logger.error(error)
      msg.channel.send('Couldn\'t get comic from xkcd.com. Please try later.')
    } else {
      msg.channel.send(response.img)
    }
  })
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['xkcd', 'comics'],
  permLevel: 0
}

exports.help = {
  name: 'comic',
  description: 'Displays random comic from xkcd.com',
  usage: 'comic'
}
