const Discord = require('discord.js')

exports.run = (logger, client, msg, args) => {
  let reason = args.slice(1).join(' ')
  let user = msg.mentions.users.first()
  let modlog = client.channels.find('name', 'mod-log')
  if (!modlog) return msg.reply('I cannot find a mod-log channel')
  if (reason.length < 1) return msg.reply('You must supply a reason for a warning')
  if (msg.mentions.users.size < 1) {
    return msg.reply('You must mention someone to warn them')
  }

  logger.info(`${user.username}#${user.discriminator} warned by ${msg.author.username}#${msg.author.discriminator}`)
  const embed = new Discord.RichEmbed()
    .setColor(0x00AE86)
    .setTimestamp()
    .addField('Action:', 'Warning')
    .addField('User:', `${user.username}#${user.discriminator}`)
    .addField('Reason', reason)
    .addField('Moderator', `${msg.author.username}#${msg.author.discriminator}`)
  return client.channels.get(modlog.id).send('', {embed: embed})
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 2
}

exports.help = {
  name: 'warn',
  description: 'Issue a warning to mentioned user',
  usage: 'warn <mention>'
}
