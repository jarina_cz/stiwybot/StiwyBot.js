const ms = require('ms')
exports.run = (logger, client, msg, args) => {
  if (!client.lockit) client.lockit = []
  let time = args.join(' ')
  let validUnlocks = ['release', 'unlock']
  if (!time) return msg.reply('You must set a duration for the lockdown in either hours, minutes or seconds')

  if (validUnlocks.includes(time)) {
    msg.channel.overwritePermissions(msg.guild.id, {
      SEND_MESSAGES: null
    }).then(() => {
      msg.channel.send('Lockdown lifted')
      clearTimeout(client.lockit[msg.channel.id])
      delete client.lockit[msg.channel.id]
    }).catch((err) => {
      logger.error(err)
    })
  } else {
    msg.channel.overwritePermissions(msg.guild.id, {
      SEND_MESSAGES: false
    }).then(() => {
      msg.channel.send(`Channel locked down for ${ms(ms(time), {long: true})}`)
        .then(() => {
          client.lockit[msg.channel.id] = setTimeout(() => {
            msg.channel.overwritePermissions(msg.guild.id, {
              SEND_MESSAGES: null
            }).then(msg.channel.send('Lockdown lifted'))
              .catch(err => logger.error(err))
            delete client.lockit[msg.channel.id]
          }, ms(time))
        }).catch(err => logger.error(err))
    })
  }
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['ld'],
  permLevel: 2
}

exports.help = {
  name: 'lockdown',
  description: 'This will lock a channel down for the set duration, be it in hours, minutes or seconds',
  usage: 'lockdown <duration>'
}
