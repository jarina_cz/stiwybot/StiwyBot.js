exports.run = (logger, client, msg, args) => {
  let messageCount = 1
  let params = args[0]
  if (params === 'all') {
    messageCount = 50 // set limit to default value
  } else {
    messageCount = parseInt(params) + 1
  }
  if (isNaN(messageCount) || messageCount < 2) {
    msg.reply(`Usage: 'purge <number|all>'`)
    return
  }
  msg.channel.fetchMessages({limit: messageCount})
    .then((messages) => {
      msg.channel.bulkDelete(messages)
        .catch((err) => {
          msg.channel.send('' + err)
        })
    })
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['deleteAll', 'delete-all'],
  permLevel: 2
}

exports.help = {
  name: 'purge',
  description: 'Purges X amount of messages from a given channel',
  usage: 'purge <number|all>'
}
