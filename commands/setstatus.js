exports.run = (logger, client, msg, args) => {
  let statuses = ['online', 'idle', 'invisible', 'dnd']
  let newStatus = args[0]
  if (statuses.indexOf(newStatus) === -1) {
    newStatus = 'online'
  }
  client.user.setStatus(newStatus)
  msg.channel.send(`I've set my status to '${args}'`)
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 2
}

exports.help = {
  name: 'setstatus',
  description: 'Set status of a bot',
  usage: 'setstatus <online|idle|invisible|dnd>'
}
