exports.run = (logger, client, msg, args) => {
  if (args.length < 1) return msg.channel.send(`Don't know this emote, choose one of <${Object.keys(emotes).join('|')}>`)
  let emote = emotes[args[0]]
  if (emote) return msg.channel.send(emote)
  else return msg.channel.send(`Don't know this emote, choose one of <${Object.keys(emotes).join('|')}>`)
}

const emotes = {
  flip: '(╯°□°）╯︵ ┻━┻',
  unflip: '┬─┬ ノ( ゜-゜ノ)',
  shrug: '¯\\_(ツ)_/¯'
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 0
}

exports.help = {
  name: 'emote',
  description: 'Send an emote',
  usage: `emote <${Object.keys(emotes).join('|')}>`
}
