exports.run = (logger, client, msg, args) => {
  let voiceChannel = msg.member.voiceChannel
  if (!voiceChannel || voiceChannel.type !== 'voice') {
    msg.channel.send('You must be connected to a voice channel for me to join you.')
  } else if (msg.guild.voiceConnection) {
    msg.channel.send('I\'m already in a voice channel.')
  } else {
    msg.channel.send('Joining...')
    voiceChannel.join()
      .then(() => {
        msg.channel.send(`Joined channel ${voiceChannel.name}`)
      })
      .catch((err) => {
        msg.channel.send('' + err)
      })
  }
}

exports.conf = {
  enabled: process.env.NODE_ENV !== 'production',
  guildOnly: false,
  aliases: [],
  permLevel: 0
}

exports.help = {
  name: 'join',
  description: 'Joins your active voice channel',
  usage: 'join'
}
