const Discord = require('discord.js')
exports.run = (logger, client, msg, params) => {
  let reason = params.slice(1).join(' ')
  let user = msg.mentions.users.first()
  let muteRole = client.guilds.get(msg.guild.id).roles.find('name', 'muted')
  let modlog = client.channels.find('name', 'mod-log')
  if (!modlog) return msg.reply('I cannot find a mod-log channel')
  if (!muteRole) return msg.reply('I cannot find a mute role')
  if (reason.length < 1) return msg.reply('You must supply a reason for a mute')
  if (msg.mentions.users.size < 1) {
    return msg.reply('You must mention someone to mute them')
  }

  const embed = new Discord.RichEmbed()
    .setColor(0x00AE86)
    .setTimestamp()
    .addField('Action:', 'Un/Mute')
    .addField('User:', `${user.username}#${user.discriminator}`)
    .addField('Moderator', `${msg.author.username}#${msg.author.discriminator}`)

  if (!msg.guild.member(client.user).hasPermission('MANAGE_ROLES_OR_PERMISSIONS')) {
    return msg.reply('I do not have the correct permissions')
  }

  if (msg.guild.member(user).roles.has(muteRole.id)) {
    msg.guild.member(user).removeRole(muteRole).then(() => {
      client.channels.get(modlog.id).send('', {embed: embed})
    })
  } else {
    msg.guild.member(user).addRole(muteRole).then(() => {
      client.channels.get(modlog.id).send('', {embed: embed})
    })
  }
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 2
}

exports.help = {
  name: 'mute',
  description: 'Mutes or unmutes a mentioned user',
  usage: 'mute <mention>'
}
