exports.run = (logger, client, msg, args) => {
  let game = args.join(' ').trim()
  client.user.setGame(game)
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 2
}

exports.help = {
  name: 'setgame',
  description: 'Set active game of a bot',
  usage: 'setgame <game name> to set\nsetgame   - to clear'
}
