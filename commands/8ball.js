exports.run = (logger, client, msg, args) => {
  let question = args.slice(1).join(' ')
  if (!question) return msg.reply('You need to ask a question')
  const possibleAnswers = [
    'It is certain',
    'It is decidedly so',
    'Without a doubt',
    'Yes definitely',
    'You may rely on it',
    'As I see it, yes',
    'Most likely',
    'Outlook good',
    'Yes.',
    'Signs point to yes',
    'Reply hazy, try again',
    'Ask again later',
    'Better not tell you now',
    'Cannot predict right now',
    'Concentrate and ask again',
    'Don\'t count on it',
    'My reply is no',
    'My sources say no',
    'Outlook not so good',
    'Very doubtful'
  ]
  let answer = possibleAnswers[Math.floor(Math.random() * possibleAnswers.length) + 1]
  return msg.reply(`Magic 8ball says: ${answer}.`)
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 0
}

exports.help = {
  name: '8ball',
  description: 'Ask Magic 8ball whatever you want',
  usage: '8ball <question>'
}
